package inf101v22.trafficlight;

public class TrafficLight implements ITrafficLight {

    static enum State {
        BLANK, STOP, GETREADY, GO, HURRY
    }

    static class AlreadyStartedException extends Exception {    }

    State state;

    TrafficLight() {
        this.state = State.BLANK;
    }

    void goToNextState() {
        if (this.state == State.BLANK) {
            this.state = State.BLANK;
        }
        else if (this.state == State.STOP) {
            this.state = State.GETREADY;
        }
        else if (this.state == State.GETREADY) {
            this.state = State.GO;
        }
        else if (this.state == State.GO) {
            this.state = State.HURRY;
        }
        else if (this.state == State.HURRY) {
            this.state = State.STOP;
        }
    }

    void turnOn() throws AlreadyStartedException {
        if (this.state != State.BLANK) {
            throw new AlreadyStartedException();
        }
        this.state = State.STOP;
    }


    @Override
    public void goToGreen() {
        while (this.state != State.GO) {
            this.goToNextState();
        }
        
    }

    @Override
    public void goToRed() {
        while (this.state != State.STOP) {
            this.goToNextState();
        }
    }

    @Override
    public boolean getGreen() {
        return this.state == State.GO;
    }

    @Override
    public boolean getYellow() {
        return this.state == State.GETREADY || this.state == State.HURRY;
    }

    @Override
    public boolean getRed() {
        return this.state == State.STOP || this.state == State.GETREADY;
    }

    @Override
    public String toString() {
        return "TrafficLight[ " + this.state + " ]";
    }
    
}
